<?php

namespace Drupal\commerce_limit_subscriptions\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\commerce_cart\CartManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\commerce_store\CurrentStoreInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\commerce_cart\OrderItemMatcherInterface;
use Drupal\commerce_product\Entity\ProductVariation;

/**
 * Restrict users to add multiple subscriptions.
 *
 * @package Drupal\commerce_limit_subscriptions
 */
class LimitSubscriptions implements EventSubscriberInterface {
  use StringTranslationTrait;
  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  /**
   * The cart manager.
   *
   * @var \Drupal\commerce_cart\CartManagerInterface
   */
  protected $cartManager;
  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;
  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;
  /**
   * The order item matcher.
   *
   * @var \Drupal\commerce_cart\OrderItemMatcherInterface
   */
  protected $orderItemMatcher;

  /**
   * Constructs a new LimitSubscriptions object for LimitSubscriptions class.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\commerce_cart\CartManagerInterface $cart_manager
   *   The cart Manager.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Messenger.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The String Translation.
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\commerce_cart\OrderItemMatcherInterface $order_item_matcher
   *   The order item matcher.
   */
  public function __construct(AccountProxyInterface $currentUser, CartManagerInterface $cart_manager, CartProviderInterface $cart_provider, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, TranslationInterface $string_translation, CurrentStoreInterface $current_store, OrderItemMatcherInterface $order_item_matcher) {
    $this->currentUser = $currentUser;
    $this->cartManager = $cart_manager;
    $this->cartProvider = $cart_provider;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->stringTranslation = $string_translation;
    $this->currentStore = $current_store;
    $this->orderItemMatcher = $order_item_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
        $container->get('current_user'),
        $container->get('messenger'),
        $container->get('commerce_cart.cart_manager'),
        $container->get('commerce_cart.cart_provider'),
        $container->get('commerce_store.current_store'),
        $container->get('entity_type.manager'),
        $container->get('string_translation')
      );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[CartEvents::CART_ENTITY_ADD] = ["limitSubscriptions"];
    return $events;
  }

  /**
   * LimitSubscriptions function called on add to cart event.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $event
   *   The cart Manager.
   */
  public function limitSubscriptions(CartEntityAddEvent $event) {

    // Entity type manager service.
    // Getting subscription storage.
    $subscription_storage = $this->entityTypeManager->getStorage("commerce_subscription");
    // Getting current user id using service.
    $current_user_id = $this->currentUser->id();
    // Entity query for getting subscriptions ids for subscriptions.
    $query = $subscription_storage->getQuery()->condition("uid", $current_user_id)->condition("state", ["active"], "IN")->accessCheck(FALSE)->execute();

    $order = $event->getEntity();

    $variation = ProductVariation::load($order->Id());

    $field = $variation->hasField('subscription_type');
    $store = $this->currentStore->getStore();
    $cart = $this->cartProvider->getCart("default", $store);
    $simple_products_count = 0;
    $subscription_products_count = 0;

    foreach ($cart->getItems() as $order_item) {

      $order = $order_item->getOrder();
      $entity = $order_item->getPurchasedEntity();
      $has_field = $entity->hasField('billing_schedule');
      $quantity = $order_item->quantity->value;

      // Increment the counter for the respective product type.
      if (!empty($has_field)) {
        $subscription_products_count++;
      }
      else {
        $simple_products_count++;
      }

      if ($quantity >= 2 && !empty($has_field)) {

        $matching_order_item = $this->orderItemMatcher->match($order_item, $cart->getItems());
        if ($matching_order_item) {

          $new_quantity = 1;
          $matching_order_item->setQuantity($new_quantity);
          $matching_order_item->save();

        }
        $this->messenger->deleteAll();
        $this->messenger->addError($this->t('You have already added product subscriptions in cart.'));
        $response = new RedirectResponse('/cart');
        $response->send();
        return;
      }
    }
    if ($subscription_products_count > 1 && !empty($has_field)) {
      foreach ($cart->getItems() as $order_item) {
        $this->messenger->deleteAll();
        $this->messenger->addError($this->t('You have already added product subscriptions in cart.'));
      }
      $ord_id = $order->id();
      $order = $this->entityTypeManager->getStorage('commerce_order')->load($ord_id);
      if (!empty($order_item)) {
        $this->cartManager->removeOrderItem($order, $order_item);
        $response = new RedirectResponse('/cart');
        $response->send();
        return;
      }
    }

    if (!empty($query) && !empty($field)) {
      $this->messenger->deleteAll();
      $order_item = $event->getOrderItem();
      $orders = $this->cartProvider->getCarts();

      foreach ($orders as $order) {
        $this->cartManager->removeOrderItem($order, $order_item);
      }
      $this->messenger->addError($this->t('You have already active subscriptions.'));
    }
  }

}
