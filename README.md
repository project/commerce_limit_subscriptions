Commerce Limit Subscriptions
----------------------------

The Commerce limit Subscriptions module restricts users to buy subscription
 again if he have already active subscriptions and also restricts users to 
 add another subscriptions product if already have added subscriptons 
 product in cart.

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


REQUIREMENTS
------------

This module requires Commerce module,commerce Recurring module enabled.


INSTALLATION
------------

 *Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.


CONFIGURATION
-------------

To use:
    1. Before starting with this module make sure to enable 
    Commerce module,Commerce recurring module.
    2. Navigate to Administration > Extend > And Search 
    for Commerce limit Subscriptions Module
    3. Enable the Commerce limit Subscriptions Module.
    5. Add product variation type for subscriptions type subscription.
    6. Then add subscriptions
    7. User are restricted to buy another subscriptions 
    if he have already active subscriptions.

MAINTAINERS
-----------

- Pankaj kumar  - (https://www.drupal.org/u/pankaj_lnweb)
- Shikha Dawar  - (https://www.drupal.org/u/shikha_lnweb)
- Vivek kumar  - (https://www.drupal.org/u/vivek_lnwebworks)
- kishan kumar  - (https://www.drupal.org/u/kishanlnwebworks)
